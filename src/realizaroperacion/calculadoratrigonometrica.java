/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package realizaroperacion;

import static java.lang.Math.*;

/**
 *
 * @author Estudiantes
 */
public class calculadoratrigonometrica extends Calculadora{
    
    void seno(){
        
    resultado=sin(dato1);
    
    
    }

   void seno(boolean grado){
     
    double radian= dato1*PI/180;
    resultado=sin(radian);    
    
    }
    void coseno(){
        
    resultado=cos(dato1);
    
    
    }

   void coseno(boolean grado){
     
    double radian= dato1*PI/180;
    resultado=cos(radian);    
    
    }
   
   void tangente(){
        
    resultado=tan(dato1);
    
    
    }

   void tangente(boolean grado){
     
    double radian= dato1*PI/180;
    resultado=tan(radian);    
    
    }
}
